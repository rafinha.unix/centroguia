describe("routes: login", async () => {

  const Promise = require("bluebird");
  const request = require("supertest");
  const server = require('../app');

  let generateTokenRequestData;
  let generateTokenResponseData;
  let findUserByLoginData;


  jest.mock("../service/user");
  const serviceUser = require("../service/user");

  serviceUser.findUserByLogin = jest.fn().mockImplementation((user) => {
    return new Promise((resolve) => {
      resolve(findUserByLoginData);
    });
  });

  jest.mock("jsonwebtoken");
  const jwt = require('jsonwebtoken');

  jwt.sign = jest.fn().mockImplementation((payload, secret) => {
    return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YTNiMDIxNzJkOGE4ODdkZmVlMGU4MWIiLCJuYW1lIjoiUmFmYWVsIiwibG9naW4iOiJyZnVuaXgiLCJwYXNzd29yZCI6ImliYzNjYXIhIiwiZW1haWwiOiJyYWZpbmhhLnVuaXhAZ21haWwuY29tIiwiX192IjowLCJzdG9yZXMiOltdLCJhY3RpdmUiOnRydWUsImNyZWF0ZURhdGUiOiIyMDE3LTEyLTIxVDAwOjM2OjM5LjgwOFoiLCJpYXQiOjE1MTkyNjY1MjZ9.2c_7iXGiwf2-6xu3YRAFS-YhDsxx0PK6iy9hxNqDMc8";
  });



  jest.mock("../service/login");
  const serviceLogin = require("../service/login");

  serviceLogin.generateToken = jest.fn().mockImplementation((user) => {
    return new Promise((resolve) => {
      resolve(generateTokenResponseData);
    });
  });


  jest.mock("../service/mongo");
  const mongo = require("../service/mongo");
  mongo.openConn = jest.fn().mockImplementation(() => {});

  beforeEach(() => {
    generateTokenRequestData = {
      "login": "rfunix",
      "password": "ibc3car!"
    };

    generateTokenResponseData = {
      "message": "Login Success!",
      "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YTNiMDIxNzJkOGE4ODdkZmVlMGU4MWIiLCJuYW1lIjoiUmFmYWVsIiwibG9naW4iOiJyZnVuaXgiLCJwYXNzd29yZCI6ImliYzNjYXIhIiwiZW1haWwiOiJyYWZpbmhhLnVuaXhAZ21haWwuY29tIiwiX192IjowLCJzdG9yZXMiOltdLCJhY3RpdmUiOnRydWUsImNyZWF0ZURhdGUiOiIyMDE3LTEyLTIxVDAwOjM2OjM5LjgwOFoiLCJpYXQiOjE1MTkyNjY1MjZ9.2c_7iXGiwf2-6xu3YRAFS-YhDsxx0PK6iy9hxNqDMc8"
    };

    findUserByLoginData = {
      _id: "5a3b02172d8a887dfee0e81b",
      name: 'Rafael',
      login: 'rfunix',
      password: 'ibc3car!',
      email: 'rafinha.unix@gmail.com',
      __v: 0,
      stores: [],
      active: true,
      createDate: "2017-12-21T00:36:39.808Z"
    }

  });

  afterEach(() => {
    server.close();
  });


  test("should be return token", async () => {
    const response = await request(server)
      .get('/login/')
      .query(generateTokenRequestData);

    expect(response.body).toEqual(generateTokenResponseData);
    expect(response.status).toBe(200);
  });
})
