describe("routes: user", async () => {
  const Promise = require("bluebird");
  const request = require("supertest");
  const server = require("../app");

  let createUserRequestData;
  let createUserResponseData;
  let token;

  //test mocks

  jest.mock("../service/user");
  const serviceUser = require("../service/user");

  serviceUser.createUser = jest.fn().mockImplementation((ctx) => {
    return new Promise((resolve) => {
      resolve(createUserResponseData);
    });
  });

  jest.mock("../service/mongo");
  const mongo = require("../service/mongo");
  mongo.openConn = jest.fn().mockImplementation(() => {});

  beforeEach(() => {

    // test data
    createUserRequestData = {
      "name": "Rafael",
      "login": "rfunix",
      "password": "ibc3car!",
      "email": "rafinha.unix@gmail.com",
      "stores": []
    };

    createUserResponseData = {
      "ok": true,
      "data": {
        "message": "User created!",
        "data": {
          "__v": 0,
          "name": "Rafael",
          "login": "rfunix",
          "password": "ibc3car!",
          "email": "rafinha.unix@gmail.com",
          "_id": "5a3b02172d8a887dfee0e81b",
          "stores": [],
          "active": true,
          "createDate": "2017-12-21T00:36:39.808Z"
        }
      },
      "version": "1.0.0",
      "now": "2017-12-21T00:36:39.955Z"
    };

    token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lc3MiLCJhZG1pbiI6dHJ1ZX0.DxFMMp0X9AZHUKTlvoqYcbNk1zGpqi3P-dI6JsKlA4U";

  });

  afterEach(() => {
    server.close();
  });

  test("should be response create user", async () => {
    const response = await request(server)
      .post('/user')
      .send(createUserRequestData)
      .set("Authorization", token)
    expect(response.body.data).toEqual(createUserResponseData);
    expect(response.status).toBe(200);
  });



});
