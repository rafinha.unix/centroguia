describe("routes: product", async () => {

  const Promise = require("bluebird");
  const request = require("supertest");
  const server = require("../app");

  let createProductRequestData;
  let createProductResponseData;
  let findProductByIdResponseData;
  let productId;
  let findAllProductsResponseData;
  let updateProductResponseData;
  let updateProductRequestData;
  let token;

  // Test Mocks
  jest.mock("../service/product");
  const productService = require("../service/product");

  productService.createProduct = jest.fn().mockImplementation((ctx) => {
    return new Promise((resolve) => {
      resolve(createProductResponseData);
    });
  });

  productService.findProductById = jest.fn().mockImplementation((ctx) => {
    return new Promise((resolve) => {
      resolve(findProductByIdResponseData);
    });
  });

  productService.findAllProducts = jest.fn().mockImplementation((ctx) => {
    return new Promise((resolve) => {
      resolve(findAllProductsResponseData);
    });
  });

  productService.updateProduct = jest.fn().mockImplementation((ctx) => {
    return new Promise((resolve) => {
      resolve(updateProductResponseData);
    });
  });

  jest.mock("../service/mongo");
  const mongo = require('../service/mongo');
  mongo.openConn = jest.fn().mockImplementation(() => {});

  beforeEach(() => {

    // Test Data

    token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lc3MiLCJhZG1pbiI6dHJ1ZX0.DxFMMp0X9AZHUKTlvoqYcbNk1zGpqi3P-dI6JsKlA4U";

    updateProductRequestData = {
      "name": "bobo",
      "nicks": [{
        "nickName": "bobo"
      }, {
        "nickName": "bobo"
      }],
      "images": [{
        "path": "/bobo/bobo2.jpg"
      }]
    };

    updateProductResponseData = {
      "ok": true,
      "data": {
        "_id": "5a29eb8f65cbeadadfdf9680",
        "name": "bobo",
        "__v": 0,
        "active": true,
        "createDate": "2017-12-08T01:31:59.299Z",
        "images": [{
          "path": "/bobo/bobo2.jpg",
          "_id": "5a29eba165cbeadadfdf9684"
        }],
        "nicks": [
          "bobo",
          "bobo",
        ]
      },
      "version": "1.0.0",
      "now": "2017-12-08T01:32:38.454Z"
    };

    findAllProductsResponseData = {
      "ok": true,
      "data": [{
          "_id": "5a29e0b682be38d2de636f25",
          "name": "RAFAEL",
          "__v": 0,
          "active": true,
          "createDate": "2017-12-08T00:45:42.595Z",
          "images": [{
            "path": "/teste/test3.jpg",
            "_id": "5a29e0b682be38d2de636f26"
          }],
          "nicks": [
            "bobo",
            "bobo",
          ]
        },
        {
          "_id": "5a29e1ddc59e04d38c2a947b",
          "name": "RAFAEL",
          "__v": 0,
          "active": true,
          "createDate": "2017-12-08T00:50:37.560Z",
          "images": [{
            "path": "/teste/test3.jpg",
            "_id": "5a29e1ddc59e04d38c2a947c"
          }],
          "nicks": [
            "bobo",
            "bobo",
          ]
        },
        {
          "_id": "5a29e3d3ff47e0d44cc3a92b",
          "name": "RAFAEL",
          "__v": 0,
          "active": true,
          "createDate": "2017-12-08T00:58:59.787Z",
          "images": [{
            "path": "/teste/test3.jpg",
            "_id": "5a29e3d3ff47e0d44cc3a92c"
          }],
          "nicks": [
            "bobo",
            "bobo",
          ]
        },
        {
          "_id": "5a29e5f4d82b67d6138121d6",
          "name": "RAFAEL",
          "__v": 0,
          "active": true,
          "createDate": "2017-12-08T01:08:04.282Z",
          "images": [{
            "path": "/teste/test3.jpg",
            "_id": "5a29e5f4d82b67d6138121d7"
          }],
          "nicks": [
            "bobo",
            "bobo",
          ]
        }
      ],
      "version": "1.0.0",
      "now": "2017-12-08T01:30:23.555Z"
    };

    createProductRequestData = {
      "name": "Product Test",
      "nicks": ["testudo", "Testao"],
      "images": [{
        "path": "/teste.img"
      }, {
        "path": "/teste2.img"
      }]
    };

    createProductResponseData = {
      "message": "Product created!",
      "data": {
        "__v": 0,
        "name": "Product Test",
        "_id": "5a31642113e51b0da092ddc0",
        "active": true,
        "createDate": "2017-12-13T17:32:17.295Z",
        "images": [{
            "path": "/teste.img",
            "_id": "5a31642113e51b0da092ddc2"
          },
          {
            "path": "/teste2.img",
            "_id": "5a31642113e51b0da092ddc1"
          }
        ],
        "nicks": [
          "testudo",
          "Testao"
        ]
      }
    };

    findProductByIdResponseData = {
      "ok": true,
      "data": {
        "_id": "5a31642113e51b0da092ddc0",
        "name": "Product Test",
        "__v": 0,
        "active": true,
        "createDate": "2017-12-13T17:32:17.295Z",
        "images": [{
            "path": "/teste.img",
            "_id": "5a31642113e51b0da092ddc2"
          },
          {
            "path": "/teste2.img",
            "_id": "5a31642113e51b0da092ddc1"
          }
        ],
        "nicks": [
          "testudo",
          "Testao"
        ]
      },
      "version": "1.0.0",
      "now": "2017-12-13T18:41:43.215Z"
    };

    productId = "5a31642113e51b0da092ddc0";

  });

  afterEach(() => {
    server.close();
  });

  test("should be response product create", async () => {

    const response = await request(server)
      .post("/product")
      .send(createProductRequestData)
      .set("Authorization", token);

    expect(response.status).toBe(200);
    expect(response.body.data).toEqual(createProductResponseData);
  });

  test("should be response product find by id", async () => {
    const response = await request(server)
      .get(`/product/${productId}`)
      .set("Authorization", token);

    expect(response.status).toBe(200);
    expect(response.body.data).toEqual(findProductByIdResponseData);
  });

  test("should be respond find all products", async () => {
    const response = await request(server)
      .get('/product')
      .set("Authorization", token);

    expect(response.status).toBe(200);
    expect(response.body.data).toEqual(findAllProductsResponseData);
  });

  test("should be respond update product", async () => {
    const response = await request(server)
      .put('/product')
      .send(updateProductRequestData)
      .set("Authorization", token);

    expect(response.status).toBe(200);
    expect(response.body.data).toEqual(updateProductResponseData);
  });
});
