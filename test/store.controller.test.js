describe("routes: store", async () => {

  const Promise = require("bluebird");
  const request = require('supertest');
  const server = require("../app");

  let createStoreRequestData;
  let createStoreResponseData;
  let storeId;
  let findNearStoreRequestData;
  let findByIdStoreResponseData;
  let findNearStoreResponseData;
  let updateStoreRequestData;
  let updateStoreResponseData;
  let token;


  // Test Mocks
  jest.mock("../service/store");
  const serviceStore = require("../service/store");

  serviceStore.createStore = jest.fn().mockImplementation((ctx) => {
    return new Promise((resolve) => {
      resolve(createStoreResponseData);
    });
  });

  serviceStore.findStoreById = jest.fn().mockImplementation((ctx) => {
    return new Promise((resolve) => {
      resolve(findByIdStoreResponseData);
    });
  });

  serviceStore.findNearStoresByProductName = jest.fn().mockImplementation((query) => {
    return new Promise((resolve) => {
      resolve(findNearStoreResponseData);
    });
  });

  serviceStore.updateStore = jest.fn().mockImplementation((ctx) => {
    return new Promise((resolve) => {
      resolve(updateStoreResponseData);
    });
  });

  jest.mock("../service/mongo");
  const mongo = require('../service/mongo');
  mongo.openConn = jest.fn().mockImplementation(() => {});


  beforeEach(() => {


    // Test data


    token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lc3MiLCJhZG1pbiI6dHJ1ZX0.DxFMMp0X9AZHUKTlvoqYcbNk1zGpqi3P-dI6JsKlA4U";

    updateStoreRequestData = {
      "name": "loja Cabre jacare",
      "address": "RUA mamaaaa",
      "city": "Cabreúva",
      "cep": "13315-000",
      "operatingTime": {
        "init": "13:00",
        "end": "15:00"
      },
      "images": [{
        "path": "/bobo/tio.jpg",
        "_id": "5a2c62b6c7499d0bdb548091"
      }],
      "phones": [
        "11975702707",
        "45281314"
      ],
      "loc": [-47.0649243, -23.2456184],
      "products": ["5a3868103d61a863f39fbd2f", "5a3474a40299bb12fc1b7ef5"]
    };

    updateStoreResponseData = {
      "ok": true,
      "data": {
        "message": "Store Updated",
        "result": {
          "_id": "5a386acb167dd865794705a7",
          "name": "loja Cabre jacare",
          "address": "RUA mamaaaa",
          "city": "Cabreúva",
          "cep": "13315-000",
          "__v": 0,
          "products": [
            "5a3868103d61a863f39fbd2f"
          ],
          "loc": [-47.0649243, -23.2456184],
          "phones": [
            "11975702707",
            "45281314"
          ],
          "images": [{
            "path": "/bobo/tio.jpg",
            "_id": "5a2c62b6c7499d0bdb548091"
          }],
          "active": true,
          "createDate": "2017-12-19T01:26:35.256Z"
        }
      },
      "version": "1.0.0",
      "now": "2017-12-19T01:31:09.531Z"
    };

    findNearStoreResponseData = {
      "ok": true,
      "data": [{
        "_id": "5a34813945532d200e4c9454",
        "name": "loja shazam jundiai",
        "address": "RUA mamaaaa",
        "city": "Cabreúva",
        "cep": "13315-000",
        "__v": 0,
        "products": [
          "5a34812645532d200e4c9452"
        ],
        "loc": [-46.897884, -23.1907653],
        "phones": [
          "11975702707",
          "45281314"
        ],
        "images": [{
          "path": "/bobo/tio.jpg",
          "_id": "5a2c62b6c7499d0bdb548091"
        }],
        "active": true,
        "createDate": "2017-12-16T02:13:13.249Z"
      }],
      "version": "1.0.0",
      "now": "2017-12-16T02:40:31.275Z"
    };

    findNearStoreRequestData = {
      productName: "bobo",
      lng: -47.1367011,
      lat: -23.3058194
    };

    storeId = "5a2c62b6c7499d0bdb548091";
    findByIdStoreResponseData = {
      "ok": true,
      "data": {
        "_id": "5a2e6c2283c0602f82cf321d",
        "name": "loja  minas gerais",
        "address": "RUA mamaaaa",
        "city": "Cabreúva",
        "cep": "13315-000",
        "__v": 0,
        "loc": [-46.7491485, -23.2681737],
        "phones": [
          "11975702707",
          "45281314"
        ],
        "images": [{
          "path": "/bobo/tio.jpg",
          "_id": "5a2c62b6c7499d0bdb548091"
        }],
        "active": true,
        "createDate": "2017-12-11T11:29:38.562Z"
      },
      "version": "1.0.0",
      "now": "2017-12-15T01:24:50.019Z"
    };

    createStoreRequestData = {
      "name": "loja  minas gerais",
      "address": "RUA mamaaaa",
      "city": "Cabreúva",
      "cep": "13315-000",
      "operatingTime": {
        "init": "13:00",
        "end": "15:00"
      },
      "images": [{
        "path": "/bobo/tio.jpg",
        "_id": "5a2c62b6c7499d0bdb548091"
      }],
      "phones": [
        "11975702707",
        "45281314"
      ],
      "loc": [-46.7491485, -23.2681737]
    };

    createStoreResponseData = {
      "ok": true,
      "data": {
        "message": "Store created!",
        "data": {
          "__v": 0,
          "name": "loja  minas gerais",
          "address": "RUA mamaaaa",
          "city": "Cabreúva",
          "cep": "13315-000",
          "_id": "5a2e6c2283c0602f82cf321d",
          "loc": [-46.7491485, -23.2681737],
          "phones": [
            "11975702707",
            "45281314"
          ],
          "images": [{
            "path": "/bobo/tio.jpg",
            "_id": "5a2c62b6c7499d0bdb548091"
          }],
          "active": true,
          "createDate": "2017-12-11T11:29:38.562Z"
        }
      },
      "version": "1.0.0",
      "now": "2017-12-11T11:29:38.564Z"
    };
  });

  afterEach(() => {
    server.close();
  });

  test("should be respond store create", async () => {

    const response = await request(server)
      .post('/store')
      .send(createStoreRequestData)
      .set("Authorization", token);

    expect(response.body.data).toEqual(createStoreResponseData);
    expect(response.status).toBe(200);

  });

  test("should be response store find by id", async () => {

    const response = await request(server)
      .get(`/store/${storeId}`)
      .set("Authorization", token);

    expect(response.status).toBe(200);
    expect(response.body.data).toEqual(findByIdStoreResponseData);
  });

  test("should be response stores that result of find by product and location", async () => {

    const response = await request(server)
      .get('/store/near/')
      .query(findNearStoreRequestData)
      .set("Authorization", token);

    expect(response.body.data).toEqual(findNearStoreResponseData);
    expect(response.status).toBe(200);
  });

  test("should be response store update", async () => {

    const response = await request(server)
      .put('/store')
      .send(updateStoreRequestData)
      .set("Authorization", token);

    expect(response.body.data).toEqual(updateStoreResponseData);
    expect(response.status).toBe(200);
  });
});
