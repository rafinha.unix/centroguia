const Store = require("../model/Store");
const productService = require("./product");
const Promise = require("bluebird");
const _ = require("lodash");
const mongoose = require('mongoose');

module.exports.createStore = (store) => {
  return new Promise((resolve, reject) => {
    Store.create({
      name: store.name,
      address: store.address,
      city: store.city,
      cep: store.cep,
      operatingTime: store.operatingTime,
      images: store.images,
      phones: store.phones,
      loc: store.loc,
      products: store.products
    }).then((result) => {
      resolve({
        message: 'Store created!',
        data: result
      });
    }).catch((err) => {
      reject(err);
    })
  });
};

module.exports.findStoreById = (storeId) => {
  return new Promise((resolve, reject) => {
    Store.findById(storeId)
      .then((result) => {
        resolve(result);
      })
      .catch((err) => {
        reject(Error('Failed to find Store by Id'));
      });
  });
};

module.exports.findNearStoresByProductName = (query) => {
  return new Promise((resolve, reject) => {
    productService.
    findProductByName(query.productName)
      .then((products) => getProductsId(products))
      .then((ids) => {
        const mongoQuery = {
          $and: [{
            loc: {
              $near: [parseFloat(query.lng),
                parseFloat(query.lat)
              ]
            }
          }, {
            products: {
              $in: ids
            }
          }]
        };
        Store.find(mongoQuery).then((result) => {
          resolve(result);
        });
      });
  }).catch((err) => reject(err));
};

const getProductsId = (products) => {
  return prodIds = _.map(products, (prod) => {
    return prod._id.toString();
  });
};

module.exports.updateStore = (store) => {
  return new Promise((resolve, reject) => {
    const searchByName = {
      name: store.name
    };
    const update = {
      name: store.name,
      address: store.address,
      city: store.city,
      cep: store.cep,
      operatingTime: store.operatingTime,
      images: store.images,
      phones: store.phones,
      loc: store.loc,
      products: store.products
    };

    Store.findOneAndUpdate(searchByName, update)
      .then((result) => {
        resolve({
          message: 'Store Updated',
          result
        });
      })
      .catch((err) => {
        reject(Error("Failed to update store"));
      });
  });
};
