const User = require("../model/User");
const Promise = require("bluebird");

module.exports.createUser = (user) => {
  return new Promise((resolve, reject) => {
    User.create({
      name: user.name,
      login: user.login,
      password: user.password,
      email: user.email,
      stores: user.stores
    }).then((result) => {
      resolve({
        message: 'User created!',
        data: result
      });
    }).catch((err) => {
      reject(err);
    });
  })
};

module.exports.findUserByLogin = (user) => {
  return new Promise((resolve, reject) => {
    User.findOne({
        'login': user.login
      })
      .then((result) => {
        resolve(result);
      })
      .catch((err) => {
        reject(err);
      })
  })
};
