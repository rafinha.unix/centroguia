const userService = require('./user');
const jwt = require('jsonwebtoken');

module.exports.generateToken = (user) => {
  return new Promise((resolve, reject) => {
    userService.findUserByLogin(user)
      .then((result) => {
        var token = jwt.sign(result.toObject(), process.env.JWT_SECRET);
        resolve({
          message: "Login Success!",
          token: token
        });
      }).catch((err) => {
        reject(err);
      });
  });
};
