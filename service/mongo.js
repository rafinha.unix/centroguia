const mongoose = require('mongoose');

/*
    Mongoose Config
*/

module.exports.openConn = () => {
  mongoose.Promise = require('bluebird')
  mongoose
    .connect('mongodb://127.0.0.1:27017', {
      useMongoClient: true
    })
    .then((response) => {
      console.log('mongo connection created')
    })
    .catch((err) => {
      console.log("Error connecting to Mongo")
      console.log(err);
    });
};
