const Product = require("../model/Product");
const Promise = require("bluebird");

module.exports.createProduct = (product) => {
  return new Promise((resolve, reject) => {
    Product.create({
      name: product.name,
      nicks: product.nicks,
      images: product.images,
    }).then((result) => {
      resolve({
        message: 'Product created!',
        data: result
      });
    }).catch((err) => {
      reject(Error('Product Failed to create'))
    });
  })
}

module.exports.findProductById = (productId) => {
  return new Promise((resolve, reject) => {
    Product.findById(productId)
      .then((result) => {
        resolve(result);
      })
      .catch((err) => {
        reject(Error('Failed to find Product by Id'));
      });
  });
};

module.exports.findAllProducts = () => {
  return new Promise((resolve, reject) => {
    Product.find({})
      .then((result) => {
        resolve(result);
      })
      .catch((err) => {
        reject(Error('Failed to find all Products'));
      });
  });
};

module.exports.updateProduct = (product) => {
  return new Promise((resolve, reject) => {
    const searchByName = {
      name: product.name
    }
    const update = {
      name: product.name,
      nicks: product.nicks,
      images: product.images,
    };
    Product.findOneAndUpdate(searchByName, update)
      .then((result) => {
        resolve(result);
      })
      .catch((err) => {
        reject(Error('Failed to update product'));
      });
  });
};

module.exports.findProductByName = (name) => {
  return new Promise((resolve, reject) => {
    var re = new RegExp(`${name}`);
    const product = Product.find({
      $or: [{
        "name": {
          $regex: re
        }
      }, {
        nicks: {
          $in: [re]
        }
      }]
    }).then((success) => {
      resolve(success)
    }).catch((err) => {
      reject(err)
    });
  })
}
