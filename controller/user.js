const Router = require("koa-router");
const userService = require("../service/user");
const routers = new Router();

const createUser = async (ctx) => {
  const message = await userService.createUser(ctx.request.body);
  ctx.body = message;
};

routers.post('/user', createUser);

module.exports.userRoutes = routers.routes();
