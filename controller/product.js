const Router = require("koa-router");
const productService = require("../service/product");
const routers = new Router();


const createProduct = async (ctx) => {
  const message = await productService.createProduct(ctx.request.body);
  ctx.body = message;
};

const findProductById = async (ctx) => {
  const product = await productService.findProductById(ctx.params.id);
  ctx.body = product;
};

const findAllProducts = async (ctx) => {
  const products = await productService.findAllProducts();
  ctx.body = products;
};

const updateProduct = async (ctx) => {
  const updateProduct = await productService.updateProduct(ctx.request.body);
  ctx.body = updateProduct;
};

routers.post('/product', createProduct);
routers.get('/product/:id', findProductById);
routers.get('/product', findAllProducts);
routers.put('/product', updateProduct);

module.exports.productRoutes = routers.routes();
