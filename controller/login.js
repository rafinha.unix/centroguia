const Router = require("koa-router");
const loginService = require("../service/login");

const routers = new Router();

const userLogin = async (ctx) => {
  const login = await loginService.generateToken(ctx.query);
  ctx.body = login;
};

routers.get('/login', userLogin);
module.exports.loginRoutes = routers.routes();
