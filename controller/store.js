const Router = require("koa-router");
const storeService = require("../service/store");
const routers = new Router();

const createStore = async (ctx) => {
  const message = await storeService.createStore(ctx.request.body);
  ctx.body = message;
};

const findStoreBydId = async (ctx) => {
  const message = await storeService.findStoreById(ctx.params.id);
  ctx.body = message;
};

const findNearStoresByProductName = async (ctx) => {
  const message = await storeService.findNearStoresByProductName(ctx.query);
  ctx.body = message;
};

const updateStore = async (ctx) => {
  const message = await storeService.updateStore(ctx.request.body);
  ctx.body = message;
};

routers.get('/store/near/', findNearStoresByProductName);
routers.post('/store', createStore);
routers.get('/store/:id', findStoreBydId);
routers.put('/store', updateStore);

module.exports.storeRoutes = routers.routes();
