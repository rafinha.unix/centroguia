const koa = require('koa');
const convert = require('koa-convert'); // convert midd legados para novos com async
const bodyParser = require('koa-bodyparser'); // Valida o body da requisicao
const logger = require('koa-logger'); //logger
const koaRes = require('koa-res'); // Formata resposta
const mongo = require('./service/mongo');
const productController = require('./controller/product');
const storeController = require('./controller/store');
const userController = require('./controller/user');
const loginController = require('./controller/login');
const jwt = require('./middleware/jwt');
const errorsMid = require('./middleware/errors');

const app = new koa();

//Login
app.use(loginController.loginRoutes);

// 401 error handling
app.use(errorsMid.unathorizedError);

app.use(jwt);

// 500 internal error handling
app.use(errorsMid.internalError);

// 404 error handling
app.use(errorsMid.badRequestError);

// Create mongo conn
mongo.openConn();

// logging
app.use(logger());

// body parsing
app.use(bodyParser());

// format response as JSON CHECK DEBUG { debug: true} in prod
app.use(convert(koaRes({
  debug: false
})));

app.use(productController.productRoutes);
app.use(storeController.storeRoutes);
app.use(userController.userRoutes);

const server = app.listen(3000).on("error", err => {
  console.error(err);
});

module.exports = server;
