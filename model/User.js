const mongoose = require("mongoose");

const UserSchema = mongoose.Schema = {
  name: String,
  login: String,
  password: String,
  email: String,
  createDate: {
    type: Date,
    default: Date.now
  },
  active: {
    type: Boolean,
    default: true
  },
  stores: []
};

module.exports = mongoose.model("User", UserSchema);
