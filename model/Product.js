const mongoose = require("mongoose");

const ProductSchema = mongoose.Schema = {
  name: String,
  nicks: [],
  images: [{
    path: String
  }],
  createDate: {
    type: Date,
    default: Date.now
  },
  active: {
    type: Boolean,
    default: true
  }
};

module.exports = mongoose.model("Product", ProductSchema);
