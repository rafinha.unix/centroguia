const mongoose = require("mongoose");

const StoreSchema = mongoose.Schema = {
  name: String,
  address: String,
  city: String,
  cep: String,
  operating: {
    init: String,
    end: String
  },
  createDate: {
    type: Date,
    default: Date.now
  },
  active: {
    type: Boolean,
    default: true
  },
  images: [{
    path: String
  }],
  phones: [],
  loc: [],
  products: []
};

module.exports = mongoose.model("Store", StoreSchema);
